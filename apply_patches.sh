#!/bin/bash

# Variables
name="geary"
version="3.36.2.pinephone"
# Script
tar -xzvf ${name}-*.tar.gz
original_folder=$(ls -d ${name}-* | grep -v "tar.gz")
mv $original_folder ${name}-${version}
cd ${name}-${version}
for i in $(find debian/patches/ -name "*.patch"); do patch -p1 < $i; done
cd ..
tar -czvf ${name}-${version}.tar.gz ${name}-${version}
